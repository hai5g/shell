#!/bin/bash
# author: Nguyen Cong Hai
# created: 2016-08-01
# License: please see the attached LICENSE file
#
# This is my wrapper around git.


set -u


prHelp () {
    sed -n '/\#\# MAIN:/,/\<esac\>/p' "${BASH_SOURCE}" | grep -E '\)|[^#][#]{3}[^#]' 
}

_git_sh__branch_merge_to_local_master () {
    ## Source: https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging
    #
    ## if there is conflict (i.e. 2 different changes to same part
    ## from different branches) , Git won’t let you switch
    ## branches -> workaround: stashing and commit amending

    git checkout  master
    ## switch to master

    git merge $*
    ## merge local branch into master
}

_git_sh__current_branch_name () {
    git branch | grep -E '^\*' | sed --expression 's/^\*[[:space:]]\+//'
}

_git_sh__branch_create_and_switch () {
    git branch $* && git checkout $*
}

### debug flag
if [ -n "${_H_DEBUG_MODE_+x}" ] ; then
    set -x ; shopt -s extdebug
else
    while [ $# -gt 0 ] ; do ## NOTE: --debug should be the FIRST flag
	case "$1" in
	    -d|--debug) set -x ; shopt -s extdebug; _H_DEBUG_MODE_='t'; shift 1 ;;
	    *) break ;;
	esac
    done
fi

if [ $# -eq 0 ] ; then 
    prHelp 
    exit
fi    

## MAIN:
args="${1}" ; shift 1

case "${args}" in
    ## DONT CHANGE FLAGS PREFIX '-' or '--': this differentiate my flag with the stock git's flag [stock git flag has no prefix '-' or '--']
    
    ### commit a few files
    -rc|--add-some-files-and-commit) # <filename>[<filename2>...]
	git add "$@" && git commit "$@"
	;;    

    ### commit all files in current dir
    -am|--add-all-and-commit) # no args
	git add . && git commit
	;;

    ### restore a file to its commit number
    -fr|--restore-a-file) # <commit-num> <file-name>
	git checkout $*
	;;
    
    ### edit the commit message
    -em|--commit-reedit-message)
	git commit --amend
	;;

    ### list files in a commit
    -lc|--list-files-in-a-commit) # <commit-hash-id>
	git diff-tree --no-commit-id --name-only -r "$@"
	;;

    ### pretty print 1 liner for log
    --log)
	if [ $# -eq 0 ] ; then
	    git log --pretty=oneline
	else
	    git log --pretty=oneliner -- "$@"
	fi
	;;
    
    ### list files being staged for commit
    -ls|--list-files-being-staged)
	git diff --cached --name-status
	;;
    
    ### list files being tracked in this repo
    -lr|--list-files-in-repo)
	git ls-tree -r master --name-only
	;;
    
    ### adding remote repo from https://github.com/
    -ra|--repo-add-remote) # <github-username>/<project-name>.git
	git remote add origin https://github.com/$*
	;;

    ### pushing the thing to remote repo (ie github.com if you have
    ### define the remote one as such)
    --push-remote) # no args
	git commit -a && git push -u origin HEAD:master ;;

    ### create a local branch <branch-name>, but staying in the
    ### current branch instead of switching to the new branch.
    -bc|--branch-create-no-switch)
	git branch $*
	;;
    
    ### create a local branch <branch-name>
    -bcs|--branch-create-and-switch) # <branch-name>
	_git_sh__branch_create_and_switch $*
	;;

    ### switch to a branch <branch-name>
    -bs|--branch-switch) # <branch-name>
	git checkout $*
	;;
    ### merge to local master and preserve all logs in the branch
    -bm|--branch-merge-to-local-master) # <branch-name>
	_git_sh__branch_merge_to_local_master $*
	;;

    ### flush entire current branch into master + reset current branch to blank slate
    -bf|--branch-flush-to-local-master)
	cur_branch_name="$(_git_sh__current_branch_name)"
	
	if [ "$cur_branch_name" == "master" ] ; then
	    echo " WARNING: Already in 'master' branch" >&2
	    exit 1
	fi

	set -e
	
	_git_sh__branch_merge_to_local_master "$cur_branch_name"

	git branch -d "$cur_branch_name"

	_git_sh__branch_create_and_switch "$cur_branch_name"
	
	;;
    
    -br|--branch-merge-to-remote-master) # <branch-name>
	printf "steps to do (sequentially):
	 git checkout master
	 git pull origin master
	 git merge <your branch name>
	 [resolving conflict and all]
	 git push origin master
"
	;;
    
    -bd|--branch-delete) # <branch-name>
	git branch -d $*
	;;

    ### merge the last CONSECUTIVE commits
    -ml|--merge-last-commits) #<number of the last CONSECUTIVE commits>
	git reset --soft HEAD~"$1"
	git commit --amend
	;;
    
    ### merge any 2 commits
    ## reference: http://stackoverflow.com/questions/2563632/how-can-i-merge-two-commits-into-one accessed 2017-01-09. archived: ~/doc/git./merge/how-can-i-merge-two-commits-into-one accessed 2017-01-09
    -ma|--merge-any-2-commits) #<once in the commit editor, select p(pick) <old_commit> and s(squash) <newer commit>
	git rebase --interactive HEAD~2
	;;
    
    ## attempt to fix merging conflict (i.e. 2 different changes to same part
    ## from different branches)
    -mc|--merge-fix-conflict) # <no args>
	printf "steps to do (sequentially):
git status
git mergetool [used to manually edit the conflict]
git status [chec the repo after the fix]
git commit [if you are happy with the status]
"
	;;
    ### view a log of a file 
    --log) # <filename>
	git log -- $*
	;; 

    -h|--help) prHelp
	       exit
	       ;;

    *) echo "option not valid: ${args}"
       exit 1
       ;;
esac
