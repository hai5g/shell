#!/usr/bin/env bash
# @author: Nguyen Cong Hai
# @created: 2015-10-02
# @version: 1.2.1
# License: AGPLv3
#
# This is my wrapper around xrandr

initialOnlyScreen='' # non-null if initially there is only 1 screen active

# * Internal variables

# ** Error exit codes
_ERRSYNTAX=3

MARKER_FIFO="/tmp/nch_shade.fifo"


_quitErr_ () {
    # perform some steps prior to quit with error:

    # if --verbatim: don't print out the prefix "$0: " before the message:
    case "$1" in
        --verbatim) shift 1 ;;
        *) printf "$0: " >&2 ;;
    esac

    # print the main message:
    echo -e "$1\n" >&2

    # handle exit code:
    case "$#" in
        1) exit 1 ;;
        2) exit "$2" ;;
        *) echo "$0->${BASH_SOURCE}: wrong number of args" >&2 ; exit 2 ;;
    esac

} # _quitErr_



parseDisplay () {
    ## args: null string | space-separated display names
    ## dependencies: initialOnlyScreen initialActiveScreens
    if (echo "$*" | grep -Eq '^[[:space:]]*$') ; then # if args are blank/spaces only
        [ -z "$initialOnlyScreen" ] && _quitErr_ "multiple screens active -> please specify display names to operate on"
        echo "$initialOnlyScreen"
    else
        for eachDisplay; do
            local screenShortname="$eachDisplay"
            [ -z "$screenShortname" ] && _quitErr_ "expecting operand: VGA/LVDS/... etc"

            local selectedScreen="$(echo "${initialActiveScreens}" | grep -F "$screenShortname")"

            [ -z "$screenShortname" ] && _quitErr_ "cannot find display $screenShortname among detected active displays:\n${initialActiveScreensNRes}\n"

            echo "$eachDisplay"

        done
    fi
}

printHelp () {
    sed -n '/\<case\>/,/\<esac\>/p' "${BASH_SOURCE}" | sed -n "/^[ \t]\+[^()]\+)/p"
}


if [ $# -eq 0 ] ; then
    printHelp
    exit $_ERRSYNTAX
fi


[ $# -eq 0 ] && exit $immRetval  # exit with latest return code if there's nothing else to be done

## ops on connected AND activated (has * in xrandr) output:

initialArgs="$*" # used this to avoid being overshadowed inside the do-while loop and against being destroyed when I call eval set -- ... somewhere else

initialActiveScreensNRes="$( \
xrandr -q \
  | awk '/[^s]connected|\*/ ' \
  | sed 's/^\([^\ \t]\+\)[\ \t]\+connected.*$/\1/g' \
  | awk ' {$1=$1};1 ' \
  | awk ' /\*/ { print x " " $0}; { x=$0 } ' \
)" # output will be like VGA1 1024x768 75.08 75.03* 60.00

initialActiveScreens="$(printf "%s" "${initialActiveScreensNRes}" | awk '{print $1}')"

if [ $(echo "${initialActiveScreens}" | wc -l) -eq 1 ] ; then
    initialOnlyScreen="$initialActiveScreens"
else
    initialOnlyScreen=''
fi

[ -z "$initialActiveScreensNRes" ] && _quitErr_ "ERROR:no active output found. Exit now.\n"

## parse the flags into seperate operations (UNCHAINing the flag into individual lines):
finalcmd='xrandr'
while read -r line ; do {
    eval set -- "$line"
    [ $# -eq 0 ] && break
    args="$1" ; shift 1

    case "$args" in
        --list-active-screens-and-res) echo "${initialActiveScreensNRes}" ; exit ;; # list active screen and their associated resolutions
        --list-active-screens) echo "${initialActiveScreens}" ; exit ;; # list active screen without their associated resolution
        ## DONT CHANGE -off into --off: used to differentiate this from stock xrandr flag
	--off) # [displayname|...] # turn off a display (display is the exact name of the display, e.g. VGA-1, VGA1, HDMI...
            ## DONT FUNCTIONALIZE this: avoid duplicating specifying flag syntax (one for here and one for the function):
            [ -n "$initialOnlyScreen" ] && _quitErr_ "This is the only screen active. \
To turn this off (i.e. there is no screen on), \
please call the command\n \
\t'command -p xrandr --output $initialOnlyScreen --off"

            for i in $(parseDisplay "$*" ) ; do
                finalcmd+=" --output ${i} --off"
            done
            ;;
	--which-brand) # must be preceded with --output
	    echo "IMPLEMENTATION STARTED, NOT YET COMPLETED" >&2
	    exit 1
	    xrandr --properties | grep EDID:
	    ;;
        -bri|--brightness) # <brightness-percentage> [displayname1|...]
            ## ALSO SEE other methods for this at end of this code
            ## DONT ARGS ORDER: flexible args at the end - don't change this order
            ## DONT FUNCTIONALIZE this: avoid duplicating specifying flag syntax (one for here and one for the function):

            bri="$1" ; shift 1
            for i in $(parseDisplay "$*" ) ; do
                finalcmd+=" --output $i --brightness $bri"
            done
            ;;
        --resolution)
	    # <1024|1150|1152|1280|...> <display|blank if only one display> # switch to another resoulution
            ## DONT ARGS ORDER: flexible args at the end - don't change this order
            ## DONT FUNCTIONALIZE this: avoid duplicating specifying flag syntax (one for here and one for the function):
            ## parse resolution
            res="$1" ; shift 1
            for eachScreen in $(parseDisplay "$*" ) ; do
                finalcmd+=" --output $eachScreen --mode $res"
            done
            ;;
	--only-one-display)
	    #* turning off laptop screen if VGA screen active:
	    #** put into subshell to localize the variable:	    
	    connectedScreen="$(xrandr | grep -E '[[:space:]]+connected[[:space:]]' | awk '{print $1}')"
	    if [ $(wc -l <<< "${connectedScreen}" ) -ge 2 ] ; then
		finalcmd="xrandr --output "$(head -1 <<< "${connectedScreen}")" --off"
	    fi	    
	    ;;
	--install-mode) # <output (eg VGA1)> <mode-width eg 1024> <mode-height-eg 768> <mode-rate eg 75.0>
	    if [ $# -ne 4 ] ; then
		printf " [ERROR]:${BASH_SOURCE}: expecting exactly 4 args, got $#:\n" >&2
		echo " [ERROR]:" ${@} >&2
		exit 1
	    fi
	    
	    readonly _output_="$1" ; shift 1
	    readonly _width_="$1" ; shift 1
	    readonly _height_="$1" ; shift 1
	    readonly _rate_="$1" ; shift 1

	    # MUST set after _width_ and _height_ etc
	    readonly _mymode_="my${_width_}x${_height_}x${_rate_}" # the starting char is letter instead of number to be sure the mode name is valid
	    # =================================
	    # method 3 (WORKING slackware 14.2)
	    # =================================
	    # check if the modename already exist
	    readonly _mode_exist_="$(xrandr | fgrep "$_mymode_")"
	    if [ -n "$_mode_exist_" ] ; then
		printf " [ERROR]:${BASH_SOURCE}: mode already exist\n  ${_mode_exist_} \n" >&2
		exit 1
	    fi
	    
	    set -e
	    xrandr --newmode "$_mymode_" $(cvt $_width_ $_height_ $_rate_  | egrep 'Modeline' | sed --expression 's/^.*Modeline[[:space:]]\+[^[:space:]]\+[[:space:]]\+\(.*\)$/\1/')
	    xrandr --addmode $_output_ "$_mymode_"

	    xrandr --output $_output_ --mode $_mymode_
	    ;;
	-h|--help) printHelp
		   exit 0
		   ;;
	*) finalcmd+=" ${initialArgs}"
	   break
	   ;;
    esac
} ; done < <(echo "$*" | awk 'BEGIN { RS="[ \t]+[-]{1,2}"} {print}')
echo -e "Command to be executed:\n\t${finalcmd}" >&2
eval "${finalcmd}"
exit
